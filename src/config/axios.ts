import axios from "axios";

export default axios.create({
  baseURL: `https://pizza-crud-back.herokuapp.com/`,
});
