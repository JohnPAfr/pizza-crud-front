import { AxiosError, AxiosResponse } from "axios";
import React, { useEffect, useState } from "react";
import { useHistory } from "react-router";
import axios from "../../config/axios";
import { useAppContext } from "../../contexts/AppContext";
import { User } from "../../models/User";

import "./Login.css";

type ResponseData = {
  jwt: string;
  user: User;
};
type ResponseError = {
  error: string;
  type: string;
};

type nullableBoolean = boolean | null;

export default function Login() {
  const history = useHistory();
  const { error, setToken, setUser, setError } = useAppContext();
  const [connected, setConnected] = useState<nullableBoolean>(null);
  const [username, setUsername] = useState("");

  useEffect(() => {
    async function checkConnection() {
      try {
        const check = await axios.get("/hello");
        if (check) setConnected(check.data === "hello");
      } catch {
        setConnected(false);
      }
    }
    checkConnection();
  }, []);

  useEffect(() => {
    setToken(null);
  }, []);

  const handleSubmit = async (e) => {
    e.preventDefault();
    const data = {
      username: e.target.username.value,
      password: e.target.password.value,
    };
    if (!data.username && !data.password) return;
    try {
      const response: AxiosResponse<ResponseData, any> = await axios.post(
        "/login",
        data
      );

      const { jwt, user } = response.data;
      setToken(jwt);
      setUser(user);
      setError(null);
      history.push(`/orders?token=${jwt}`);
    } catch (err) {
      const { error, type } = err.response.data;
      setError({ error, type });
    }
  };

  const handleUserNameChange = (e) => {
    e.preventDefault();
    setUsername(e.target.value);
  };

  useEffect(() => {
    console.log({ error });
  }, [error]);

  return (
    <div className="Login">
      <form onSubmit={handleSubmit}>
        <div className="inputGroup">
          <label htmlFor="username">Username</label>
          <input
            className={error?.type === "username" ? "error" : ""}
            id="username"
            type="text"
            placeholder="John Doe"
            value={username}
            onChange={handleUserNameChange}
          ></input>
          {error?.type === "username" && <p className="error">{error.error}</p>}
        </div>
        <div className="inputGroup">
          <label htmlFor="username">Password</label>
          <input
            className={error?.type === "password" ? "error" : ""}
            id="password"
            type="text"
            placeholder="Jz6JA26v"
          ></input>
          {error?.type === "password" && <p className="error">{error.error}</p>}
        </div>
        <button type="submit">Login</button>
        <h1 className={connected ? "success" : "danger"}>
          {connected === true && "connected"}
          {connected === false && "not connected"}
        </h1>
      </form>
    </div>
  );
}
