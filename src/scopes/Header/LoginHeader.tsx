import React from "react";

const LoginHeader = () => {
  return (
    <header className={`header loginHeader`}>
      <img className="logo" src="logo.png" alt="Smartpizza" />
    </header>
  );
};

export default LoginHeader;
