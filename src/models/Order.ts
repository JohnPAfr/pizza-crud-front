export type Order = {
  id: string;
  username: string;
  quantity: number;
  date: Date;
};

export type OrdersState = {
  orders: Order[];
  quantity: number;
};
