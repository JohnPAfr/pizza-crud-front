export type Error = {
  error: string;
  type: string;
};
